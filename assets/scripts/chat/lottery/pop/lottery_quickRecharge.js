/**
 * 由红包改为了彩券
 */
cc.Class({
    extends: cc.Component,

    properties: {
       
        labLottery:{
            default:null,
            type:cc.Label
        },
        //订单金额
        labOrderMoney:{
            default:null,
            type:cc.Label,
        },
        //余额
        labBalance:{
            default:null,
            type:cc.Label
        },
        //实际支付
        labPayMoney:{
            default:null,
            type:cc.Label
        },

        //彩券支付
        rtCouponsPay:cc.RichText,
        //彩券选择页
        pfCouponsPackPop:cc.Prefab,
        //彩豆
        rtGoldPay:cc.RichText,
        //实际支付彩豆
        rtGoldDec:cc.RichText,
        //彩豆说明
        pfCouponsExPop:cc.Prefab,
        //是否可使用彩豆
        selGoldTg:cc.Toggle,

        ndNeedPay:cc.Node,
        ndPayWays:cc.Node,
        ndTrue:cc.Node,
        
        _orderMoney:0,//订单金额
        _lotteryID:0,//彩种编号
        _buyType:0,//投注类型

        _couponsNums:0,//彩券数
        _isUseCoupons:false,//是否使用彩券
        _curCouponsID:0,//彩券id
        _couponsMoney:0,//彩券金额
        _couponsPayMoney:0,//彩券支付金额

        _userBanl:0,//用户余额
        _needPayMoney:0,//还需要支付金额
        _balance:0,//支付的金额

        _needPayGold:0,//需要支付的彩豆
        _goldToMoney:0,//彩豆可以兑换的金额

        _fpCouponsPop:null,
        _payCallback:null,
        _data:null//彩券详情

    },

    // use this for initialization
    onLoad: function () {
        this.rtCouponsPay.node.on('onCouponsClick', function ( event ) {
           
        });

        this.updateUser();
    },

    //查看彩豆说明
    btnCheck:function(){
        var canvas = cc.find("Canvas");
        var couponsExPop = cc.instantiate(this.pfCouponsExPop);
        canvas.addChild(couponsExPop);

        couponsExPop.getChildByName("spBg").on(cc.Node.EventType.TOUCH_END, function (event) {
            couponsExPop.getComponent("Page").backAndRemove();
        }, this);

        couponsExPop.getChildByName("content").getChildByName("btnTrue").on(cc.Node.EventType.TOUCH_END, function (event) {
            couponsExPop.getComponent("Page").backAndRemove();
        }, this);
    },

//初始化
    init:function(lotteryId, isuseid, moneyAmount,buytype, isUseCoupons,payCallback){
        this._payCallback = payCallback;
        this._orderMoney = parseFloat(moneyAmount);//订单金额
        this._needPayMoney = this._orderMoney;//需要支付的金额
        this._lotteryID = lotteryId;
        this._buyType = buytype;
        this._isUseCoupons = isUseCoupons;
        var lotteryDecStr = LotteryUtils.getLotteryTypeDecByLotteryId(lotteryId)+" 第"+isuseid.toString()+"期";
        var orderAmountStr = this._orderMoney.toFixed(2).toString()+"元"; //订单
        this.labLottery.string = lotteryDecStr;
        this.labOrderMoney.string = orderAmountStr;
    },

    //刷新余额支付显示
    payShow:function(){
        if(this._needPayMoney > 0)
        {
            var baTemp = this._userBanl - this._needPayMoney;
            if(baTemp >=0)
            {
                this._balance = this._needPayMoney.toFixed(2);
                this.labBalance.string = "-" + this._needPayMoney.toFixed(2).toString() + "元";
                this.labPayMoney.string = "0.00元";//还需支付
                this._needPayMoney = 0;
                this.ndTrue.active = true;
                this.ndNeedPay.active = false;
                this.ndPayWays.active = false;
            }
            else
            {
                this._balance = this._userBanl.toFixed(2);
                this._needPayMoney = 0-baTemp;
                this.labBalance.string = "-" + this._userBanl.toFixed(2).toString() + "元";//余额抵扣
                this.labPayMoney.string = baTemp.toFixed(2).toString() + "元";//还需支付
                this.ndTrue.active = false;
                this.ndNeedPay.active = true;
                this.ndPayWays.active = true;
            }
        }
        else
        {
            this._needPayMoney = 0;
            this._balance = 0;
            this.labPayMoney.string = "0.00元";
            this.labBalance.string = "-0.00元";
            this.ndTrue.active = true;
            this.ndNeedPay.active = false;
            this.ndPayWays.active = false;
        }
    },

    //彩卷支付显示
    couponsShow:function(){
        var myTemp = this._orderMoney - this._couponsMoney - this._needPayGold;
        
        if(myTemp>0)
        {
            this._couponsPayMoney = this._couponsMoney;
            this._needPayMoney = myTemp;
        }
        else
        {
            var mony = this._orderMoney-this._needPayGold;
            if(mony==0)
            {
                this._couponsPayMoney = 0;
            }
            else
            {
                this._couponsPayMoney = mony;
            }
            
            this._needPayMoney = 0;
        }
        if(this._curCouponsID == -1)
            this.rtCouponsPay.string = "<color=#FFFEA9>"+this._couponsPayMoney.toFixed(2).toString()+"</c><color=#ffffff>（"+this._couponsNums+"个彩券可用）,未使用＞</color>";
        else
            this.rtCouponsPay.string = "<color=#FFFEA9>"+this._couponsPayMoney.toFixed(2).toString()+"</c><color=#ffffff>（"+this._couponsNums+"个彩券可用）＞</color>";  
    },

    //彩豆支付显示
    goldShow:function(){
        if(!this.selGoldTg.isChecked)
            return;
        var money1 = this._orderMoney - this._goldToMoney - this._couponsPayMoney;
        if(money1 > 0)
        {
            this.rtGoldDec.string = "<color=#FFFFFF>使用 </c><color=#FFFEA9>"+ this._goldToMoney*1000 +" 彩豆 </c><color=#FFFFFF>抵 </c><color=#FFFEA9>-"+this._goldToMoney+"</color><color=#FFFFFF> 元</color>";
            this._needPayGold = this._goldToMoney;
            this._needPayMoney = money1;
        }
        else
        {
            var myTemp = this._orderMoney-this._couponsPayMoney;
            this.rtGoldDec.string = "<color=#FFFFFF>使用 </c><color=#FFFEA9>"+ myTemp*1000 +" 彩豆 </c><color=#FFFFFF>抵 </c><color=#FFFEA9>-"+myTemp+"</color><color=#FFFFFF> 元</color>";
            this._needPayGold = myTemp;
            this._needPayMoney = 0;
        }
    },

    onPayTrue:function(){
        var data = [];
        if(this._couponsPayMoney > 0 && this._buyType == 0)
        {
            var couponsData = [];
            var coupons = {
                pid:this._curCouponsID,
                ba:LotteryUtils.moneytoClient(this._couponsPayMoney)
            };
            couponsData.push(coupons);
            var str = JSON.stringify(couponsData);
            data = {
                paymentType:this.getSelPayType(),
                data:str,
                payMoney:LotteryUtils.moneytoClient(this._balance),
                gold:this._needPayGold*1000,
            }
            //cc.log("支付1：",data);
        }
        else
        {
            data = {
                paymentType:this.getSelPayType(),
                data:"",
                payMoney:LotteryUtils.moneytoClient(this._balance),
                gold:this._needPayGold*1000,
            }
            //cc.log("支付2：",data);
        }
  
        this._payCallback(data);//支付成功回调
        this.onClose();
    },

    //1.余额彩豆购彩券支付 2.余额支付 3.购彩券支付 4.彩豆支付 5.余额和购彩券支付 6.余额和彩豆支付 7.彩豆和购彩券支付
    getSelPayType:function(){
        var payMy = Math.abs(parseInt(this.labBalance.string));
        if(payMy>0 && this._couponsPayMoney > 0 && this._needPayGold > 0 && this._buyType == 0)
        {
            return 1;//余额彩豆购彩券支付
        }
        else if(payMy>0 && this._couponsPayMoney > 0 && this._buyType == 0)
        {
            return 5;//余额和购彩券支付
        }
        else if(payMy>0 && this._needPayGold > 0 && this._buyType == 0)
        {
            return 6;//余额和彩豆支付
        }
        else if(this._couponsPayMoney > 0 && this._needPayGold > 0 && this._buyType == 0)
        {
            return 7;//彩豆和购彩券支付
        }
        else if(this._couponsPayMoney > 0 && this._buyType == 0)
        {
            return 3;//购彩券支付
        }
        else if(this._needPayGold > 0 && this._buyType == 0)
        {
            return 4;//彩豆支付
        }
        else (payMy>0)
        {
            return 2;//余额支付
        }
    },

    //选择彩豆抵扣
    goldTgEvent:function(toggle, customEventData){
        if(toggle.isChecked == true)
        {
            if(this._needPayMoney == 0 && this._couponsPayMoney>0 && this._balance==0)
            {
                toggle.isChecked = false;
                return;
            }

            var money1 = this._orderMoney - this._goldToMoney - this._couponsPayMoney;
            if(money1 > 0)
            {
                this.rtGoldDec.string = "<color=#FFFFFF>使用 </c><color=#FFFEA9>"+ this._goldToMoney*1000 +" 彩豆 </c><color=#FFFFFF>抵 </c><color=#FFFEA9>-"+this._goldToMoney+"</color><color=#FFFFFF> 元</color>";
                this._needPayGold = this._goldToMoney;
                this._needPayMoney = money1;
            }
            else
            {
                var myTemp = this._orderMoney-this._couponsPayMoney;
                this.rtGoldDec.string = "<color=#FFFFFF>使用 </c><color=#FFFEA9>"+ myTemp*1000 +" 彩豆 </c><color=#FFFFFF>抵 </c><color=#FFFEA9>-"+myTemp+"</color><color=#FFFFFF> 元</color>";
                this._needPayGold = myTemp;
                this._needPayMoney = 0;
            }
        }
        else
        {
            this._needPayGold = 0;
            this._needPayMoney = this._orderMoney - this._couponsPayMoney;
        }
        
        this.couponsShow();
        this.payShow();
    },

    //选择彩券
    onCouponsClick:function(){
        if(this._needPayMoney == 0 && this._needPayGold>0 && this._balance==0)
        {
            return;
        }
        if(this._isUseCoupons)//可以使用彩券
        {
            var callback = function(ret){
                this._curCouponsID = ret.couponsID;
                this._needPayMoney = this._orderMoney;
                this._couponsPayMoney =  ret.couponsMoney;
                this._couponsMoney =  ret.couponsMoney;
  
                var myTemp = this._orderMoney - this._couponsMoney ;
     
                if(myTemp>0)
                {
                    this._couponsPayMoney = this._couponsMoney;
                    this._needPayMoney = myTemp;
                }
                else
                {
                    this._couponsPayMoney = this._orderMoney;
                    this._needPayMoney = 0;
                }

                if(this._curCouponsID == -1)
                    this.rtCouponsPay.string = "<color=#FFFEA9>"+this._couponsPayMoney.toFixed(2).toString()+"</c><color=#ffffff>（"+this._couponsNums+"个彩券可用）,未使用＞</color>";
                else
                    this.rtCouponsPay.string = "<color=#FFFEA9>"+this._couponsPayMoney.toFixed(2).toString()+"</c><color=#ffffff>（"+this._couponsNums+"个彩券可用）＞</color>";    
                    
                this.goldShow();
                this.payShow();
            }.bind(this);

            var canvas = cc.find("Canvas");
            this._fpCouponsPop = cc.instantiate(this.pfCouponsPackPop);
            this._fpCouponsPop.getComponent(this._fpCouponsPop.name).init(this._data,this._curCouponsID,callback);
            canvas.addChild(this._fpCouponsPop);
        } 
    },

    //关闭
    onClose:function(event){
        this.node.getComponent("Page").backAndRemove();
    },

    //支付宝支付
    onAliPay: function()
    {
        ComponentsUtils.showTips("暂时不支持支付宝支付！");
    },

    //微信支付
    onPayWeiChat:function(){
        ComponentsUtils.showTips("暂时不支持微信支付！");
    },

    //刷新用户信息
    updateUser: function(){
        var recv = function(ret){
            if(ret.Code === 0)
            {
                User.setBalance(ret.Balance);
                User.setGoldBean(ret.Gold);
                this._userBanl = parseFloat(User.getBalance());//余额
                //彩券
                if(this._isUseCoupons)
                {
                    var recv = function recv(ret) { 
                        ComponentsUtils.unblock();
                        if(ret.Code == 0 || ret.Code == 49)
                        {
                            if(ret.Data != null)
                            {
                                this._data = ret.Data;
                                if(ret.Data.length>0)
                                {
                                    this._couponsNums = ret.Data.length;
                                    this._isUseCoupons = true;
                                    var couponsMy = LotteryUtils.moneytoServer(ret.Data[0].Balance);
                                    this._curCouponsID = ret.Data[0].CouponsID;
                                    
                                    this._couponsPayMoney =  couponsMy;
                                    this._couponsMoney = couponsMy;
                                    var myTemp = this._orderMoney - this._couponsMoney;
                                    if(myTemp>0)
                                    {
                                        this._couponsPayMoney = this._couponsMoney;
                                        this._needPayMoney = myTemp;
                                    }
                                    else
                                    {
                                        this._couponsPayMoney = this._orderMoney;
                                        this._needPayMoney = 0;
                                    }
                                    this.rtCouponsPay.string = "<color=#FFFEA9>"+this._couponsPayMoney.toFixed(2).toString()+"</c><color=#ffffff>（"+ret.Data.length+"个彩券可用）＞</color>";
                                    this.payShow();
                                    return;
                                }
                            }
                            this.payShow();
                            this.rtCouponsPay.string = "<color=#FFFEA9>0.00元</c><color=#ffffff>(无可用彩券)</color>";
                            this._isUseCoupons = false;
                        }
                        else
                        {
                            this.payShow();
                            this.rtCouponsPay.string = "<color=#FFFEA9>0.00元</c><color=#ffffff>(无可用彩券)</color>";
                            this._isUseCoupons = false;
                        }
                    }.bind(this);
            
                    var data = {
                        Token:User.getLoginToken(),
                        UserCode:User.getUserCode(),
                        OrderMoney: LotteryUtils.moneytoClient(this._orderMoney),
                        LotteryCode: this._lotteryID,
                        BuyType:this._buyType,
                    };
                    CL.HTTP.sendRequest(DEFINE.HTTP_MESSAGE.GETUSECOUPONSPAYLIST, data, recv.bind(this),"POST");    
                    ComponentsUtils.block();
                }
                else
                {
                    this.payShow();
                    this.rtCouponsPay.string = "<color=#FFFEA9>0.00元</c><color=#ffffff>(无可用彩券)</color>";
                    this._isUseCoupons = false;
                }
                //彩豆
                if(this._buyType != 0)
                {
                    this._goldToMoney = 0;
                    this.selGoldTg.enabled = false;
                    this.rtGoldPay.string = "共"+ User.getGoldBean() + ",追号无法使用抵扣。";
                }
                else
                {
                    var Gold = Math.round((User.getGoldBean() /10000) * 100) / 100;
                    var decGold = Gold + "万，可抵";
                    var gold1 = (User.getGoldBean() /1000) * 100 / 100;
                    if(gold1 !=0)
                    {
                        var b = gold1.toString().split(".");
                        if(b[0] != 0 )
                        {
                            this._goldToMoney = b[0];
                            this.rtGoldPay.string = "共" + decGold + this._goldToMoney + "元";
                        }
                        else
                        {
                            this._goldToMoney = 0;
                            this.selGoldTg.enabled = false;
                            this.rtGoldPay.string = "共"+ User.getGoldBean() + ",无法抵扣。";
                        }
                    }
                    else
                    {
                        this._goldToMoney = 0;
                        this.selGoldTg.enabled = false;
                        this.rtGoldPay.string = "无彩豆抵扣";
                    }
                }
            }
            else
            {
                ComponentsUtils.showTips(ret.Msg);
            }
        }.bind(this);
        var data = {
            Token:User.getLoginToken(),
            UserCode:User.getUserCode(),
        }
        CL.HTTP.sendRequest(DEFINE.HTTP_MESSAGE.GETUSERMONEY, data, recv.bind(this),"POST");  
    },

});
