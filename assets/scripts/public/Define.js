(function(){
    //玩法规则
    const _LOTTERYRULEK3 = cc.Enum({
        ANDVALUE: "01", //和值
        THREESAME_TONG: "02",//三同号通选
        THREESAME_DAN: "03",//三同号单选
        THREEDIFF: "04",//三不同号
        THREEDIFF_LIAN: "05",//三连号通选
        TWOSAME_FU: "06",//二同号复选
        TWOSAME_DAN: "07",//二同号单选
        TWODIFF: "08",//二不同号
    });
    const _LOTTERYRULE115 = cc.Enum({
        ANYTWO:"01",	 //11选5任选二
        ANYTHREE:"02",    //11选5任选三
        ANYFOUR:"03",    //11选5任选四
        ANYFIVE:"04",    //11选5任选五
        ANYSIX:"05",    //11选5任选六
        ANYSEVEN:"06",    //11选5任选七
        ANYEIGHT:"07",    //11选5任选八
        ZHI_ONE:"08", //11选5前一直选
        ZHI_TWO:"09",    //11选5前二直选
        ZU_TWO:"10",    //11选5前二组选
        ZHI_THREE:"11",    //11选5前三直选
        ZU_THREE:"12",    //11选5前三组选

        //特殊玩法只有202才存在
        LE_THREE:"13",//乐选3
        LE_FOUR:"14",//乐选4
        LE_FIVE:"15",//乐选5
    });

    const _LOTTERYRULEDUOTONEBALL = cc.Enum({
        STAND: "01", //双色球标准玩法
    });

    const _LOTTERYRULESUPERBIG = cc.Enum({
        STAND: "01", //超级大乐透标准玩法
        ADDTO: "02",//超级大乐透追加玩法
    });

     const _LOTTERYRULESSC = cc.Enum({
        ZHI_FIVE: "01",//时时彩五星直选
        TONG_FIVE: "02",//时时彩五星通选
        ZHI_THREE: "03",//时时彩三星直选
        ZU_THREE: "04",//时时彩三星组三
        ZU_SIX: "05",//时时彩三星组六
        ZHI_TWO: "06",//时时彩二星直选
        ZHI_TWO_ANDVALUE: "07",//时时彩二星直选和值（暂不做）
        ZU_TWO: "08",//时时彩二星组选
        ZU_TWO_ANDVALUE: "09",//时时彩二星组选和值（暂不做）
        ZHI_ONE: "10",//时时彩一星直选
        BXDS: "11",//时时彩大小单双	
    });

    const _LOTTERY_ID = cc.Enum({
        SSC_CQ:"301",
        K3_RED:"101",
        K3_WIN:"102",
        SSQ:"801",
        BIG:"901",
        L115_HUA:"201",
        L115_OLD:"202",
    });

    const _LOTTERY_RULE = cc.Enum({
        ssc_cq_zhifive:"30101" ,//五星直选
        ssc_cq_tongfive:"30102" ,//五星通选
        ssc_cq_zhithree:"30103" ,//三星直选
        ssc_cq_zuthree:"30104" ,//三星组三
        ssc_cq_zusix:"30105" ,//三星组六
        ssc_cq_zhitwo:"30106" ,//二星直选
        ssc_cq_twoandvalue:"30107" ,//二星直选和值
        ssc_cq_zutwo:"30108" ,//二星组选
        ssc_cq_zutwoandvalue:"30109" ,//二星组选和值
        ssc_cq_zhione:"30110" ,//一星
        ssc_cq_bxds:"30111" ,//大小单双

        k3_red_andvalue:"10101",//和值
        k3_red_threesametong:"10102",//三同号通选
        k3_red_threesamedan:"10103",//三同号单选
        k3_red_threediff:"10104",//三不同号
        k3_red_threelian:"10105",//三连号通选
        k3_red_twosamefu:"10106",//二同号复选
        k3_red_twosamedan:"10107",//二同号单选
        k3_red_twodiff:"10108",//二不同号

        k3_win_andvalue:"10201",//和值
        k3_win_threesametong:"10202",//三同号通选
        k3_win_threesamedan:"10203",//三同号单选
        k3_win_threediff:"10204",//三不同号
        k3_win_threelian:"10205",//三连号通选
        k3_win_twosamefu:"10206",//二同号复选
        k3_win_twosamedan:"10207",//二同号单选
        k3_win_twodiff:"10208",//二不同号

        ssq_stand:"80101",// 标准玩法
        
        big_stand: "90101",// 标准玩法
        big_addto: "90102",//追加玩法

        L115_hua_anytwo:"20101", //任选二
        L115_hua_anythree:"20102", //任选三
        L115_hua_anyfour:"20103", //任选四
        L115_hua_anyfive:"20104", //任选五
        L115_hua_anysix:"20105", //任选六
        L115_hua_anyseven:"20106", //任选七
        L115_hua_anyeight:"20107", //任选八
        L115_hua_zhione:"20108", //前一直选
        L115_hua_zhitwo:"20109", //前二直选
        L115_hua_zutwo:"20110", //前二组选
        L115_hua_zhithree:"20111", //前三直选
        L115_hua_zuthree:"20112", //前三组选

        L115_old_anytwo:"20201", //任选二
        L115_old_anythree:"20202", //任选三
        L115_old_anyfour:"20203", //任选四
        L115_old_anyfive:"20204", //任选五
        L115_old_anysix:"20205", //任选六
        L115_old_anyseven:"20206", //任选七
        L115_old_anyeight:"20207", //任选八
        L115_old_zhione:"20208", //前一直选
        L115_old_zhitwo:"20209", //前二直选
        L115_old_zutwo:"20210", //前二组选
        L115_old_zhithree:"20211", //前三直选
        L115_old_zuthree:"20212", //前三组选
        L115_old_lethree:"20213", //乐三
        L115_old_lefour:"20214", //乐四
        L115_old_lefive:"20215", //乐五
    });

    //短信验证
    const _SHORT_MESSAGE = cc.Enum({
        REGISTER:0,//注册
        FINDPWD:1,//找回密码
        CHANGEPWD:3,//修改登录密码
        CHANGEPAY:5,//修改支付密码
        CHANGEBINDPHONE:7,//绑定修改手机号码
        BINDBANK:9,//绑定银行卡
        IDENTVEIFY: 11,//身份验证
    });

    const _HTTP_MESSAGE = cc.Enum({
        STARTAPP:"/api/system/applystart",//应用启动请求
        /**
         * 
            AvatarUrl
            Balance
            FullName
            Gold
            IsCertification
            IsRobot
            IsWithdrawPwd
            Level
            Mobie
            Nick
            CouponsCount
            SpecialGroup
            Token
            Uid
            UserCode
            VIP
            GameData--
            id
            name
            amount
        */
        INIT:"/api/system/applyinitiate",//初始化应用请求(热更之后)
        /**
         * 
            Token
            IsuseNum
            BeginTime
            EndTime
            RoomCode
            BetData
            ChaseData
            UserCode
            LotteryCode
            BuyType
            Amount
            Coupons
            PaymentType
            Gold
         */
        REQUESTBET:"/api/business/bet",//投注请求
        /**
            AvatarUrl
            Balance
            FullName
            Gold
            IsCertification
            IsRobot
            IsWithdrawPwd
            Level
            Mobie
            Nick
            CouponsCount
            SpecialGroup
            Token
            Uid
            UserCode
            VIP
         */
        OTHERLOGIN:"/api/user/integratesignin",//集成登录请求
        ACCOUNTLOGIN:"/api/user/mobilesignin",//账号登录
        REGISTER:"/api/user/mobileregister",//注册请求
        GETVERIFY:"/api/system/applysendverifycode",//发送验证码请求
        WFTPAY:"/api/user/preparepaymentwft",//威富通预支付(充值)请求
        GETSERVERTIME:"/api/system/get",//取服务器时间
        GETRECURRENTISUSE:"/api/business/currentisuseinfo",//取当前期信息请求
        GETTHELOTTERYLIST:"/api/business/oflotteryrecord",//取开奖列表请求
        GETTHELOTTERYISUSE:"/api/business/oflotterydetail",//期号开奖详情
        WITHDRAWALS:"/api/user/withdrawdeposit",//提现申请请求
        IDENTITYAUTHENTICATION:"/api/user/idverification",//身份认证请求
        BINDBANKCARD:"/api/user/bindbankcard",//绑定银行卡请求
        UNBINDBANKCARD:"/api/user/unbindbankcard",//银行卡解除绑定请求
        QUICKPAY:"/api/user/verifypaymentshortcut",//快捷支付验证请求
        CHANGEPHONENUMBER:"/api/user/changemobile",//更换手机号码请求
        RESETPWDUNLOGIN:"/api/user/resetsigninpwd",//重置登录密码(未登录)请求
        RESETPWDLOGIN:"/api/user/resetpwd",//重置密码请求
        CHANGENICK:"/api/user/changenick",//编辑昵称请求
        UPLOADFILE:"/api/fileinfo/uploadfile",//上传文件请求
        GETBANKCARDLIST:"/api/user/bankcards",//银行卡列表请求
        GETDEALRECORDLIST:"/api/user/usertradingrecord",//账户明细请求
        GETORDERRECORDLIST:"/api/business/schemerecord",//取订单(方案)列表请求
        GETORDERDETAILS:"/api/business/schemedetail",//订单详情请求
        GETADDTOISUSE:"/api/business/chaseisusenum",//获取追号期号查询
        GETADDTOLIST:"/api/business/chaserecord",//追号列表请求
        GETADDTODETAILS:"/api/business/chasedetail",//追号详情请求
        CHECKVERIFY:"/api/system/verifycode",//验证码验证
        GETUSERINFO:"/api/user/QueryUserInfo",//获取用户信息 
        GETVERIFYMOBILE:"/api/user/VerifyMobile",//验证注册手机号
        GETUSERMONEY:"/api/user/QueryUserBalance",//获取用户余额
        GETLOTTERYMISS:"/api/business/NormOmission",//获取遗漏信息
        GETTRENDCHART:"/api/business/trendchart",//获取走势图信息
        GETCASHINFO:"/api/user/withdrawcount",//获取提现信息
        SETBAIDUID:"/api/user/PushIdentify",//上传本机百度推广ID
        FOLLOWBET:"/api/business/followbet",//跟投
        GETHALLOPENLIST:"/api/system/OpenAwardHome",//获取大厅开奖列表 
        GETLOOKTICKET:"/api/business/lookticketinfo",//获取出票详情
        GETADDAWARD:"/api/business/AwardPlayCode",//获取彩种加奖信息
        GETUSECOUPONSPAYLIST:"/api/business/couponspaymentlist",//获取可用彩券列表
        GETCOUPONSLIST:"/api/business/couponslist",//获取用户彩券列表
        OUTACCOUNT:"/api/user/outlogin",//退出账号
        GETEXCHANGECOUPONS:"/api/business/exchangercoupons",//兑换彩券
        GETSHAKEGAME:"/api/business/rockdice",//获取摇骰子
        POSTNEWS: "/api/business/lotanalysis",  //新闻资讯 -彩种分析列表
        POSTNEWSANALY: "/api/business/looknewsanalysis",   //新闻资讯 -彩种分析资讯
        POSTNEWSEVALUATE:"/api/business/evaluatenews",  //新闻资讯 -评价文章资讯
        EXCHANGEBEAN:"/api/user/exchangebean",//兑换彩豆
        POSTGAMERECORD: "/api/business/gamerecord",   //兑换彩豆记录
        GETTOKEN:"/api/system/sightseertoken",//获取一个临时令牌
    });


    const _RECV_CHAT_MSG_TYPE = cc.Enum({
        CHAT_PUBLIC_MemberJoin:1,//加入
        CHAT_PUBLIC_MuteList:2,//禁言
        CHAT_PUBLIC_Removed:3,//移除
        CHAT_PUBLIC_Bet:4,//投注
        CHAT_PUBLIC_Notice:5,//公告
        CHAT_PUBLIC_Award:6,//中奖

     });

     const _RECV_MSG_TYPE = cc.Enum({
        MSG_TYPE_SYS:1,//系统消息
        MSG_TYPE_USER:2,//玩家消息
        MSG_TYPE_PAO:3,//跑马灯
        MSG_TYPE_OPEN:4,//开奖消息
        MSG_TYPE_LOTSTATE:5,//列表状态
     });

        var _DEFINE = {
            SHORT_MESSAGE:_SHORT_MESSAGE,
            HTTP_MESSAGE:_HTTP_MESSAGE,
            LOTTERYRULEK3:_LOTTERYRULEK3,
            LOTTERYRULE115:_LOTTERYRULE115,
            LOTTERYRULEDUOTONEBALL:_LOTTERYRULEDUOTONEBALL,
            LOTTERYRULESUPERBIG:_LOTTERYRULESUPERBIG,
            LOTTERYRULESSC:_LOTTERYRULESSC,
            RECV_MSG_TYPE:_RECV_MSG_TYPE,
            RECV_CHAT_MSG_TYPE:_RECV_CHAT_MSG_TYPE,
            LOTTERY_RULE:_LOTTERY_RULE,
            LOTTERY_ID:_LOTTERY_ID,
        };

        window.DEFINE = _DEFINE;
} 
)();