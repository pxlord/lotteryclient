(function(){ 
    var _curIssue = "";//当前期号
    var _endTime = "";//结束时间

    function _setCurIssue(issue){
        _curIssue = issue;
    }

    function _getCurIssue(){
        return _curIssue;
    }

    function _setEndTime(endTime){
        _endTime = endTime;
    }

    function _getEndTime(){
        return _endTime
    }

    //初始化
    function _initTrentChart(lotteryid,playCode,callBack){
        var recv = function(ret){
            if(ret.Code === 0)
            {       
                ComponentsUtils.unblock();
                if(callBack != null)
                {    
                    callBack(ret.Data); 
                }    
            }
            else
            {
                ComponentsUtils.showTips(ret.Msg);
            }
        }.bind(this);
        var data = {
            LotteryCode:lotteryid,
            IsuseName:_curIssue,
            PlayCode:playCode,
        }
        CL.HTTP.sendRequest(DEFINE.HTTP_MESSAGE.GETTRENDCHART, data, recv.bind(this),"POST"); 
        ComponentsUtils.block();
    }


    var _TrentChart={
        initTrentChart:_initTrentChart,
        setCurIssue:_setCurIssue,
        getCurIssue:_getCurIssue,
        setEndTime:_setEndTime,
        getEndTime:_getEndTime,
    }

    window.TrentChart=_TrentChart; 
} 
)();
