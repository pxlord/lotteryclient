/**
 * !#zh 支付成功组件
 * @information 期次，类型，投注金额
 */
cc.Class({
    extends: cc.Component,

    properties: {
        spHead:{
            default: null,
            type: cc.Sprite
        },

        spHeadBase:{
            default: null,
            type: cc.SpriteFrame
        },

        labName:{
            default: null,
            type: cc.Label
        },

        labMoney:{
            default:null,
            type:cc.Label
        },
        
        labIssue:{
            default:null,
            type:cc.Label
        },

        labLottyerName:{
            default:null,
            type:cc.Label
        },

        ndChese:{
            default:null,
            type: cc.Node
        },

        _data:null,  //数据信息
    },

    // use this for initialization
    onLoad: function () {
        if(this._data != null)
        {
            if(this._data.head != "" && typeof(this._data.head) != "undefined" && this._data.head !=null){
                cc.loader.load(this._data.head, function(err, tex){
                    if(tex != null){
                        var sf = new cc.SpriteFrame(tex);
                        this.spHead.spriteFrame = sf;
                        this.spHead.node.width = 127;
                        this.spHead.node.height = 127;
                    }
                    else
                    {
                        this.spHead.spriteFrame = this.spHeadBase;
                    }
                }.bind(this));
            } 
            else
            {
                this.spHead.spriteFrame = this.spHeadBase;
            }
            this.labName.string = this._data.name;
            this.labMoney.string = "投注金额：" + this._data.money + " 元";
            this.labIssue.string = this._data.isuse + " 期";
            this.labLottyerName.string = "类型：" + this._data.lotteryName;
            this.ndChese.active = this._data.buttype == 1?true:false;
        }
    },

    /** 
    * 接收支付成功数据
    * @method init
    * @param {Object} data
    */
    init:function(data){
        this._data = data;
    }
    
});
