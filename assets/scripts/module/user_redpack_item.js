cc.Class({
    extends: cc.Component,

    properties: {
        labDec:cc.Label,
        labMoney:cc.Label,
        labTime:cc.Label,
        labRange:cc.Label,
        _data:null
    },

    // use this for initialization
    onLoad: function () {
        if(this._data != null)
        {
            this.labDec.string = this._data.dec;
            this.labMoney.string = this._data.money;
            this.labTime.string = this._data.time;
            this.labRange.string = this._data.range;
        }
    },

    init:function(data){
        this._data = data;
    }
    
});
