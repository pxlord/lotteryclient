/**
 * !#zh 开奖列表项组件
 * @information 期次，时间日期，开奖信息
 */
cc.Class({
    extends: cc.Component,

    properties: {
        labIsuse:{
            default: null,
            type: cc.Label
        },
        labMsg:{
            default: null,
            type: cc.Label
        },

        outBallConetent:{
            default:null,
            type:cc.Node
        },

        _data:null //数据信息
    },

    // use this for initialization
    onLoad: function () {
        if(this._data == null )
            return;
        
        if(this._data.ballPrefab == null )
            return;

        if(this._data.msg != "")
        {
            this.labMsg.node.active = true;
            this.labMsg.string = this._data.msg;
        }

        this.labIsuse.string = this._data.isuse;
        if(this._data.ballFrames != null )
        {
            var ball = cc.instantiate(this._data.ballPrefab);
            ball.getComponent('bet_public_balls').init({
                spNums: this._data.ballFrames,
                nums:null,
            });   
            this.outBallConetent.addChild(ball);  
        }
        else if(this._data.ballNums != null)
        {
            var ball = cc.instantiate(this._data.ballPrefab);
            ball.getComponent('bet_public_balls').init({
                spNums: null,
                nums:this._data.ballNums,
            });  
            this.outBallConetent.addChild(ball);              
        }

    },

    /** 
    * 接收开奖项数据
    * @method init
    * @param {Object} data
    */
    init:function(data){
        this._data = data;
    }

});
