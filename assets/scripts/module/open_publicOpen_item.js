/**
 * !#zh开奖中心项组件
 * @information 彩种，期次时间，开奖号码
 */
cc.Class({
    extends: cc.Component,

    properties: {
        labIsuse:{
            default: null,
            type: cc.Label
        },
        labMsg:{
            default: null,
            type: cc.Label
        },

        outBallConetent:{
            default:null,
            type:cc.Node
        },

        labLotteryName:{
            default: null,
            type: cc.Label
        },

        _ballPfb:null,
        _data:null
    },

    // use this for initialization
    onLoad: function () {
        if(this._data == null )
            return;
        
        if(this._data.ballPrefab == null )
            return;

        if(this._data.msg != "")
        {
            this.labMsg.node.active = true;
            this.labMsg.string = this._data.msg;
        }

        this.labLotteryName.string = this._data.lotteryName;
        this.labIsuse.string = this._data.isuse;
        if(this._data.ballFrames != null )
        {
            this._ballPfb = cc.instantiate(this._data.ballPrefab);
            this._ballPfb.getComponent('bet_public_balls').init({
                spNums: this._data.ballFrames,
                nums:null,
            });   
            this.outBallConetent.addChild(this._ballPfb);  
        }
        else if(this._data.ballNums != null)
        {
            this._ballPfb = cc.instantiate(this._data.ballPrefab);
            this._ballPfb.getComponent('bet_public_balls').init({
                spNums: null,
                nums:this._data.ballNums,
            });  
            this.outBallConetent.addChild(this._ballPfb);              
        }
    },

    /** 
    * 接收开奖信息
    * @method init
    * @param {Object} data
    */
    init:function(data){
        this._data = data;
    },

    /** 
    * 刷新开奖信息
    * @method updataData
    * @param {Object} data
    */
    updataData:function(data){
        this._data = data;
        if(this._data == null )
            return;
    
        if(this._data.ballPrefab == null )
            return;

        if(this._data.msg != "")
        {
            this.labMsg.node.active = true;
            this.labMsg.string = this._data.msg;
        }


        this.labLotteryName.string = this._data.lotteryName;
        this.labIsuse.string = this._data.isuse;

        if(this._ballPfb == null)
            return;
        this._ballPfb.getComponent('bet_public_balls').updataData({
            spNums: this._data.ballFrames,
            nums:null,
        });   
    }

});
