cc.Class({
    extends: cc.Component,

    properties: {
        labDec:{
            default: null,
            type: cc.Label
        },

        rtNumbers:{
            default:null,
            type:cc.RichText
        },

        _data:null
    },

    // use this for initialization
    onLoad: function () {
        if(this._data != null)
        {
            this.labDec.string = this._data.dec ; 
            this.rtNumbers.string = this._data.number;
        }
    },

    init:function(ret){
        this._data = ret;
    }

});
