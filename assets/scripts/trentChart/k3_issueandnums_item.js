/**
 * !#zh 快3走势图号码走势组件 (二不同号)
 * @information 开奖号
 */
cc.Class({
    extends: cc.Component,

    properties: {
        spBgColor:{
            default:null,
            type:cc.Node
        },

        labIssue:{
            default:null,
            type:cc.Label
        },

        labOpenNums:{
            default:null,
            type:cc.Label
        },
        
        _data:null
    },

    // use this for initialization
    onLoad: function () {
        if(this._data != null)
        {
            this.spBgColor.color = this._data.color;
            var isus =  this._data.Isuse.toString();
            var isuseStr = isus.substring(isus.length-2,isus.length) + "期";
            this.labIssue.string = isuseStr;
            var nums = "";
            for(var i=0;i<this._data.openNums.length;i++)
            {
                nums += this._data.openNums[i]+" ";
            }     
            this.labOpenNums.string = nums;   
        }

    },

    /** 
    * 接收走势图号码走势信息
    * @method init
    * @param {Object} data
    */
    init:function(data){
        this._data = data;
    }
    
});
