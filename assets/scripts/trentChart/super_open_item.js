/**
 * !#zh 大乐透走势图开奖组件
 * @information  
 */
cc.Class({
    extends: cc.Component,

    properties: {
        labIsuse:{
            default: null,
            type: cc.Label
        },
        labFrontNums:{
            default:null,
            type:cc.Label
        },
        labAfterNums:{
            default:null,
            type:cc.Label
        },
        _data:null,
        _itemId: null
    },

    // use this for initialization
    onLoad: function () {
        if(this._data != null)
        {
            this.node.color = this._data.color;
            var isuseStr = this._data.Isuse.substring(this._data.Isuse.length-3,this._data.Isuse.length) + "期";
            this.labIsuse.string = isuseStr;

            var frontStr = "";
            for(var i=0;i<this._data.frontNums.length;i++)
            {
                frontStr += this.splitTwo(this._data.frontNums[i])+" ";
            }
            this.labFrontNums.string = frontStr;

            var afterStr = "";
            for(var i=0;i<this._data.afterNums.length;i++)
            {
                afterStr += this.splitTwo(this._data.afterNums[i])+" ";
            } 
            this.labAfterNums.string = afterStr;
        }
    },

    /** 
    * 将数字转换成字符串且长度为2
    * @method splitTwo
    * @param {Number} num
    * @return {String} 长度为2的字符串
    */
    splitTwo:function(num){
        return num.toString().length<2?"0"+num:num;
    },

    /** 
    * 刷新走势图开奖信息
    * @method updateData
    * @param {Object} data
    */
    updateData:function(data){
        if(data != null)
        {
            this.data = data;
            var isuseStr = this.data.Isuse.substring(this.data.Isuse.length-3,this.data.Isuse.length) + "期";
            this.labIsuse.string = isuseStr;

            var frontStr = "";
            for(var i=0;i<this.data.frontNums.length;i++)
            {
                frontStr += this.splitTwo(this.data.frontNums[i])+" ";
            }
            this.labFrontNums.string = frontStr;

            var afterStr = "";
            for(var i=0;i<this.data.afterNums.length;i++)
            {
                afterStr += this.splitTwo(this.data.afterNums[i])+" ";
            } 
            this.labAfterNums.string = afterStr;

        }
    },

    /** 
    * 接收走势图开奖信息
    * @method init
    * @param {Object} data
    */
    init:function(data){
        this._data = data;
    },

    /** 
    * 接收走势图期号列表id
    * @method onItemIndex
    * @param {Number} i
    */
    onItemIndex: function(i){
        this._itemId =i;
    }

});
